kubectl create configmap conf --from-file=./nginx.default.conf.ctmpl
kubectl create configmap cert --from-file=./cert.ctmpl
kubectl create configmap key --from-file=./key.ctmpl
kubectl create configmap ct-config --from-file=./ct-config.hcl

kubectl create configmap ct-config --from-file=./ct-config.hcl --context $prod --dry-run -o yaml | k apply -f -
